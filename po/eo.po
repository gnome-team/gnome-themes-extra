# Esperanto translation for gnome-themes-standard.
# Copyright (C) 2011 Free Software Foundation, Inc.
# This file is distributed under the same license as the gnome-themes-standard package.
# Kristjan SCHMIDT <kristjan.schmidt@googlemail.com>, 2011, 2017.
msgid ""
msgstr ""
"Project-Id-Version: gnome-themes-standard master\n"
"Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=gnome-"
"themes-standard&keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2017-03-22 18:58+0000\n"
"PO-Revision-Date: 2017-06-11 13:53+0200\n"
"Last-Translator: Kristjan SCHMIDT <kristjan.schmidt@googlemail.com>\n"
"Language-Team: Esperanto <gnome-eo-list@gnome.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Virtaal 0.7.1\n"
"X-Project-Style: gnome\n"

#: ../themes/Adwaita-dark/index.theme.in.h:1
#| msgid "Adwaita"
msgid "Adwaita-dark"
msgstr "Advajto malhela"

#: ../themes/Adwaita-dark/index.theme.in.h:2
#| msgid "There is only one"
msgid "There was only one"
msgstr "Tie nur estis unu"

#: ../themes/Adwaita/index.theme.in.h:1
msgid "Adwaita"
msgstr "Advajto"

#: ../themes/Adwaita/index.theme.in.h:2
msgid "There is only one"
msgstr "Ekzistas nur unu"

#: ../themes/HighContrast/index.theme.in.h:1
msgid "High Contrast"
msgstr "Alta kontrasto"

#: ../themes/HighContrast/index.theme.in.h:2
msgid "High contrast theme"
msgstr "Altkontrasta etoso"

#~ msgid "Default Background"
#~ msgstr "Defaŭlta fono"

#~ msgid "High Contrast Inverse"
#~ msgstr "Alta kontrasto, inversa"

#~ msgid "High contrast inverse theme"
#~ msgstr "Altkontrasta inversa etoso"

#~ msgid "Low Contrast"
#~ msgstr "Malalta kontrasto"

#~ msgid "Low contrast theme"
#~ msgstr "Malaltkontrasta etoso"
